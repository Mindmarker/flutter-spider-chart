import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:spider_chart/spider_chart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Spider Chart Example',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Spider Chart Example'),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 300,
          child: SpiderChart(
            dataList: {
              0: [
                7,
                5,
                10,
                7,
                4,
              ],
              1: [
                2,
                5,
                10,
                7,
                4,
              ]
            },
            selectedData: 0,
            maxValue: 10,
            selectedFillColor: Color.fromRGBO(33, 150, 243, 0.3),
            selectedStrokeColor: Color.fromRGBO(33, 150, 243, 1),
            strokeColor: Colors.green,
            fillColor: Colors.green,
            strokeWidth: 2,
            showIcons: true,
            showDataPoints: false,
            iconLabels: ["red", "green", "blue", "yellow", "indigo"],
            size: Size(300, 300),
            iconBackgroundColors: [
              Colors.red,
              Colors.green,
              Colors.blue,
              Colors.yellow,
              Colors.indigo,
            ],
            fallbackHeight: 300,
            fallbackWidth: 300,
            iconBackgroundHeight: 20,
            iconBackgroundWidth: 20,
            iconBackgroundRadius: Radius.circular(4.0),
            iconImage: Image.network(
              "https://banner2.kisspng.com/20180505/hee/kisspng-computer-icons-icon-design-hyperbole-objective-5aed576b7cc2e5.6078367615255038515111.jpg",
              height: 20,
              width: 20,
            ),
          ),
        ),
      ),
    );
  }
}
