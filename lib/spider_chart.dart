library spider_chart;

import 'dart:ui';
import 'package:flutter/material.dart';
import 'dart:math' show pi, cos, sin;

import 'package:flutter/material.dart' as material;

class SpiderChart extends StatefulWidget {
  final int selectedData;
  final double maxValue;
  final List<Color> dataPointColors;
  final decimalPrecision;
  final Size size;
  final double fallbackHeight;
  final double fallbackWidth;
  final Color fillColor;
  final Color strokeColor;

  final Color selectedFillColor;
  final Color selectedStrokeColor;
  final double strokeWidth;
  final bool showIcons;
  final Color iconBackgroundColor;
  final List<Color> iconBackgroundColors;
  final material.Image iconImage;
  final Color dataPointColor;
  final bool showDataPoints;
  final double iconBackgroundHeight;
  final double iconBackgroundWidth;
  final Radius iconBackgroundRadius;
  final List<String> iconLabels;
  final Map<int, List<double>> dataList;

  const SpiderChart({
    Key key,
    this.dataPointColors,
    @required this.maxValue,
    this.size = Size.infinite,
    this.decimalPrecision = 0,
    this.fallbackHeight = 300,
    this.fallbackWidth = 300,
    this.fillColor = Colors.grey,
    this.strokeColor = Colors.black12,
    this.strokeWidth = 1,
    this.iconBackgroundColor,
    this.iconBackgroundColors,
    this.showIcons = false,
    this.iconImage,
    this.dataPointColor = Colors.black,
    this.showDataPoints = true,
    this.iconBackgroundHeight = 18.0,
    this.iconBackgroundWidth = 18.0,
    this.iconBackgroundRadius = const Radius.circular(2.0),
    this.iconLabels,
    this.selectedData = 0,
    @required this.dataList,
    this.selectedFillColor = Colors.grey,
    this.selectedStrokeColor = Colors.black12,
  }) : super(key: key);

  @override
  _SpiderChartState createState() => _SpiderChartState(
        selectedData: selectedData,
        maxValue: maxValue,
        dataPointColors: dataPointColors,
        decimalPrecision: decimalPrecision,
        size: size,
        fallbackHeight: fallbackHeight,
        fallbackWidth: fallbackWidth,
        fillColor: fillColor,
        strokeColor: strokeColor,
        selectedFillColor: selectedFillColor,
        selectedStrokeColor: selectedStrokeColor,
        strokeWidth: strokeWidth,
        showIcons: showIcons,
        iconBackgroundColor: iconBackgroundColor,
        iconBackgroundColors: iconBackgroundColors,
        iconImage: iconImage,
        dataPointColor: dataPointColor,
        showDataPoints: showDataPoints,
        iconBackgroundHeight: iconBackgroundHeight,
        iconBackgroundWidth: iconBackgroundWidth,
        iconBackgroundRadius: iconBackgroundRadius,
        iconLabels: iconLabels,
        dataList: dataList,
      );
}

class _SpiderChartState extends State<SpiderChart> {
  final int selectedData;
  final double maxValue;
  final List<Color> dataPointColors;
  final decimalPrecision;
  final Size size;
  final double fallbackHeight;
  final double fallbackWidth;
  final Color fillColor;
  final Color strokeColor;
  final Color selectedFillColor;
  final Color selectedStrokeColor;
  final double strokeWidth;
  final bool showIcons;
  final Color iconBackgroundColor;
  final List<Color> iconBackgroundColors;
  final material.Image iconImage;
  final Color dataPointColor;
  final bool showDataPoints;
  final double iconBackgroundHeight;
  final double iconBackgroundWidth;
  final Radius iconBackgroundRadius;
  final List<String> iconLabels;
  final Map<int, List<double>> dataList;
  List<Offset> offsets;
  bool showIconLabel = false;
  int iconLabelActive;

  _SpiderChartState(
      {this.selectedData,
      this.maxValue,
      this.dataPointColors,
      this.decimalPrecision,
      this.size,
      this.fallbackHeight = 300,
      this.fallbackWidth = 300,
      this.fillColor,
      this.strokeColor,
      this.selectedFillColor,
      this.selectedStrokeColor,
      this.strokeWidth,
      this.showIcons,
      this.iconBackgroundColor,
      this.iconBackgroundColors,
      this.iconImage,
      this.dataPointColor,
      this.showDataPoints,
      this.iconBackgroundHeight,
      this.iconBackgroundWidth,
      this.iconBackgroundRadius,
      this.iconLabels,
      this.dataList});

  setIconPositions(List<Offset> offsets) {
    this.offsets = offsets;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
//        onTapDown: (details) {
//          RenderBox box = context.findRenderObject();
//          final offset = box.globalToLocal(details.globalPosition);
//          print({
//            "dx": offset.dx,
//            "dy": offset.dy,
//          });
//          offsets.forEach((iconOffset) {
//            double iconStartPosition =
//                (iconOffset.dx - (iconBackgroundHeight / 2));
//            double iconEndPosition =
//                (iconOffset.dx + (iconBackgroundWidth / 2));
//            double iconTopPosition =
//                (iconOffset.dy - (iconBackgroundHeight / 2));
//            double iconBottomPosition =
//                (iconOffset.dy + (iconBackgroundHeight / 2));
//
//            print({
//              "iconStartPosition": iconStartPosition,
//              "iconEndPosition": iconEndPosition,
//              "iconTopPosition": iconTopPosition,
//              "iconBottomPosition": iconBottomPosition,
//            });
//
//            if (iconStartPosition < offset.dx &&
//                iconEndPosition > offset.dx &&
//                iconTopPosition < offset.dy &&
//                iconBottomPosition > offset.dy) {
//              print("match");
//              setState(() {
//                showIconLabel = true;
//                iconLabelActive = offsets.indexOf(iconOffset);
//              });
//            }
//          });
//        },
        child: LimitedBox(
      maxWidth: widget.fallbackWidth,
      maxHeight: widget.fallbackHeight,
      child: CustomPaint(
        size: size,
        painter: SpiderChartPainter(
            maxValue,
            dataPointColors,
            decimalPrecision,
            fillColor,
            strokeColor,
            strokeWidth,
            showIcons,
            iconBackgroundColor,
            iconBackgroundColors,
            iconImage,
            dataPointColor,
            showDataPoints,
            iconBackgroundHeight,
            iconBackgroundWidth,
            iconBackgroundRadius,
            iconLabels,
            dataList,
            selectedData,
            selectedFillColor,
            selectedStrokeColor,
            setIconPositions,
            showIconLabel,
            iconLabelActive),
      ),
    ));
  }
}

class SpiderChartPainter extends CustomPainter {
  final Map<int, List<double>> dataList;
  final int selectedData;
  final double maxNumber;
  final List<Color> dataPointColors;
  final decimalPrecision;
  final Color fillColor;
  final Color strokeColor;
  final double strokeWidth;
  final bool showIcons;
  final Color iconBackgroundColor;
  final List<Color> iconBackgroundColors;
  final material.Image iconImage;
  final Color dataPointColor;
  final bool showDataPoints;
  final double iconBackgroundHeight;
  final double iconBackgroundWidth;
  final Paint spokes = Paint()..color = Colors.grey;
  final Radius iconBackgroundRadius;
  List<Offset> iconPositions = [];
  final List<String> iconLabels;
  final bool showIconLabel;
  final int iconLabelActive;
  final Color selectedFillColor;
  final Color selectedStrokeColor;
  final Function(List<Offset>) setIconPositions;

  SpiderChartPainter(
      this.maxNumber,
      this.dataPointColors,
      this.decimalPrecision,
      this.fillColor,
      this.strokeColor,
      this.strokeWidth,
      this.showIcons,
      this.iconBackgroundColor,
      this.iconBackgroundColors,
      this.iconImage,
      this.dataPointColor,
      this.showDataPoints,
      this.iconBackgroundHeight,
      this.iconBackgroundWidth,
      this.iconBackgroundRadius,
      this.iconLabels,
      this.dataList,
      this.selectedData,
      this.selectedFillColor,
      this.selectedStrokeColor,
      this.setIconPositions,
      this.showIconLabel,
      this.iconLabelActive);

  @override
  void paint(Canvas canvas, Size size) {
    Offset center = size.center(Offset.zero);

    dataList.forEach((int i, List<double> data) {
      bool isSelected = selectedData == i;
      double angle = (2 * pi) / data.length;

      var points = List<Offset>();

      for (var i = 0; i < data.length; i++) {
        var scaledRadius = (data[i] / maxNumber) * center.dy;
        var x = (scaledRadius * cos(angle * i - pi / 2)) * .7;
        var y = (scaledRadius * sin(angle * i - pi / 2)) * .7;

        points.add(Offset(x, y) + center);
      }

      paintGraphOutline(canvas, center, angle, data);
      if (showIcons) {
        addGraphIcons(canvas, center, angle, data);
      }
      if (showIconLabel && iconLabelActive != null) {
        addGraphLabel(canvas, center, angle, data, iconLabelActive);
      }
      paintDataLines(canvas, points, isSelected);
      if (showDataPoints) {
        paintDataPoints(canvas, points);
        paintText(canvas, center, points, data);
      }
    });
  }

  void paintDataLines(Canvas canvas, List<Offset> points, bool isSelected) {
    Path path = Path()..addPolygon(points, true);

    final Paint fill = Paint()
      ..color = isSelected ? selectedFillColor : fillColor
      ..style = PaintingStyle.fill;
    final Paint stroke = Paint()
      ..color = isSelected ? selectedStrokeColor : strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;

    canvas.drawPath(
      path,
      fill,
    );

    canvas.drawPath(path, stroke);
  }

  void paintDataPoints(Canvas canvas, List<Offset> points) {
    for (var i = 0; i < points.length; i++) {
      canvas.drawCircle(
          points[i],
          4.0,
          Paint()
            ..color =
                dataPointColors != null ? dataPointColors[i] : dataPointColor);
    }
  }

  void paintText(
      Canvas canvas, Offset center, List<Offset> points, List<double> data) {
    var textPainter = TextPainter(textDirection: TextDirection.ltr);
    for (var i = 0; i < points.length; i++) {
      String s = data[i].toStringAsFixed(decimalPrecision);
      textPainter.text =
          TextSpan(text: s, style: TextStyle(color: Colors.black));
      textPainter.layout();
      if (points[i].dx < center.dx) {
        textPainter.paint(
            canvas, points[i].translate(-(textPainter.size.width + 5.0), 0));
      } else if (points[i].dx > center.dx) {
        textPainter.paint(canvas, points[i].translate(5.0, 0));
      } else if (points[i].dy < center.dy) {
        textPainter.paint(
            canvas, points[i].translate(-(textPainter.size.width / 2), -20));
      } else {
        textPainter.paint(
            canvas, points[i].translate(-(textPainter.size.width / 2), 4));
      }
    }
  }

  void paintGraphOutline(
      Canvas canvas, Offset center, double angle, List<double> data) {
    var outline = List<Offset>();

    for (var i = 0; i < data.length; i++) {
      var x = (center.dy * cos(angle * i - pi / 2)) * .7;
      var y = (center.dy * sin(angle * i - pi / 2)) * .7;
      outline.add(Offset(x, y) + center);
      canvas.drawLine(center, outline[i], spokes);
    }

    outline.add(outline[0]);

    canvas.drawPoints(PointMode.polygon, outline, spokes);

    canvas.drawCircle(center, 2, spokes);
  }

  void addGraphLabel(Canvas canvas, Offset center, double angle,
      List<double> data, int iconLabelActive) {
    var textPainter = TextPainter(textDirection: TextDirection.ltr);

    var x = (center.dy * cos(angle * iconLabelActive - pi / 2)) * .7;
    var y = (center.dy * sin(angle * iconLabelActive - pi / 2)) * .7;

    var offsetX = 30 * cos(angle * iconLabelActive - pi / 2);
    var offsetY = 30 * sin(angle * iconLabelActive - pi / 2);

    String s = iconLabels[iconLabelActive];
    textPainter.text = TextSpan(text: s, style: TextStyle(color: Colors.black));
    textPainter.layout();

    Offset position = (Offset(x, y) + center + Offset(offsetX, offsetY));

    double translateX = -(textPainter.size.width / 2);
    double translateY = (textPainter.size.height / 2) + 4;

    textPainter.paint(canvas, position.translate(translateX, translateY));
  }

  void addGraphIcons(
      Canvas canvas, Offset center, double angle, List<double> data) {
    for (var i = 0; i < data.length; i++) {
      var x = (center.dy * cos(angle * i - pi / 2)) * .7;
      var y = (center.dy * sin(angle * i - pi / 2)) * .7;

      var offsetX = 30 * cos(angle * i - pi / 2);
      var offsetY = 30 * sin(angle * i - pi / 2);

      iconPositions.add((Offset(x, y) + center + Offset(offsetX, offsetY)));

      Paint objectivePaint = new Paint()
        ..style = PaintingStyle.fill
        ..color = iconBackgroundColors != null
            ? iconBackgroundColors[i]
            : iconBackgroundColor != null ? iconBackgroundColor : Colors.black;

      canvas.drawRRect(
          RRect.fromRectAndRadius(
              new Rect.fromCenter(
                  center: (Offset(x, y) + center + Offset(offsetX, offsetY)),
                  width: iconBackgroundWidth,
                  height: iconBackgroundHeight),
              iconBackgroundRadius),
          objectivePaint);
    }
    setIconPositions(iconPositions);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
